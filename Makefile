.PHONY: all clean

targets=$(shell for file in `find . -maxdepth 1 -name '*.tex' -type f -printf "%f\n"| sed 's/\..*/\.pdf/'`; do echo "$$file "; done;)

all: $(targets)

%.pdf: %.tex
	        latexmk -pdf -pdflatex="pdflatex -interaction=nonstopmode" -shell-escape -use-make $<

Presentation.pdf: beamerthemeEindhoven.sty

clean:
	        latexmk -CA

debug:
	@echo $(targets)
	@echo figures: $(shell grep -P 'includegraphics\[.*\]{\K(.*)(?=})' -ro *.tex)
